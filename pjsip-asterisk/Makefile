#
# Copyright (C) 2014 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=pjsip-asterisk
PKG_VERSION:=2.1
PKG_RELEASE=1

PKG_SOURCE_PROTO:=git
PKG_SOURCE_URL:=https://github.com/asterisk/pjproject.git
PKG_SOURCE_SUBDIR:=$(PKG_NAME)-$(PKG_VERSION)
PKG_SOURCE_VERSION:=217740d99457fc8492d3a68f90fa25a52bd8eca9
PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION)-$(PKG_SOURCE_VERSION).tar.gz

PKG_INSTALL:=1
PKG_BUILD_PARALLEL:=1

PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)

include $(INCLUDE_DIR)/package.mk

define Package/pjsip-asterisk
  SECTION:=lib
  CATEGORY:=Libraries
  URL:=http://www.pjsip.org/
  TITLE:=pjsip-asterisk
  DEPENDS:=+libuuid
endef

CONFIGURE_ARGS += \
	--build=$(GNU_HOST_NAME) \
	--host=$(GNU_TARGET_NAME) \
	--target=$(GNU_TARGET_NAME) \
	--disable-shared \
	--disable-resample \
	--disable-video \
	--disable-opencore-amr \
	--disable-floating-point \
	--disable-g711-codec \
	--disable-l16-codec \
	--disable-g722-codec \
	--disable-g7221-codec \
	--disable-gsm-codec \
	--disable-libsamplerate \
	--disable-ipp \
	--disable-ssl \
	--disable-oss \
	--disable-sound

define Build/Configure
	echo "export CFLAGS += $(FPIC) $(TARGET_CFLAGS) $(EXTRA_CFLAGS)"\
		" $(TARGET_CPPFLAGS) $(EXTRA_CPPFLAGS)"\
		> $(PKG_BUILD_DIR)/user.mak;
	echo "export LDLAGS += $(TARGET_LDFLAGS) $(EXTRA_LDFLAGS)"\
		" -lc $(LIBGCC_S) -lm"\
		>> $(PKG_BUILD_DIR)/user.mak;
	echo "export CXXFLAGS += $(FPIC) $(TARGET_CFLAGS) $(EXTRA_CFLAGS)"\
		" $(TARGET_CPPFLAGS) $(EXTRA_CPPFLAGS)"\
		>> $(PKG_BUILD_DIR)/user.mak;
	$(call Build/Configure/Default)
endef

define Build/Compile
	$(MAKE) -j1 -C $(PKG_BUILD_DIR) dep
	$(MAKE) -j1 -C $(PKG_BUILD_DIR)
endef

define Build/InstallDev
	$(INSTALL_DIR) $(1)/usr/pjsip2
	$(CP) $(PKG_INSTALL_DIR)/usr $(1)/usr/pjsip2
endef

$(eval $(call BuildPackage,pjsip-asterisk))
